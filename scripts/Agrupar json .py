import json

lista=[{
    "area": "YUMBO",
    "state": "NOT"
  },
  {
    "area": "ZARZAL",
    "state": "VCAU"
  },
  {
    "area": "PUERTO CARRENO",
    "state": "VIC"
  },
  {
    "area": "SANTA ROSALIA",
    "state": "VIC"
  }
]
resultado=dict()
for index in range(len(lista)):
     if resultado.get(lista[index]["state"],None)==None:
                    resultado[lista[index]["state"]]=[]
     resultado[lista[index]["state"]].append(lista[index]["area"])
jsonarray = json.dumps(resultado)
print(jsonarray)

